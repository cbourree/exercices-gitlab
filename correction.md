Exercice 1:
```
git clone https://gitlab.com/cbourree/exercices-gitlab.git FormationFit
cd FormationGit
```

Exercice 2:
```
git checkout exercices
git checkout -b cbo_exercices
```

Exercice 3:
```
echo "Nouvelle ligne" > fichier2.txt
git diff
git add fichier2.txt
git status
git commit -m "Message de commit"
git push origin exercices
```

Exercice 4:
```
touch fichier4.txt
rm fichier1.txt
git add .
```

Exercice 5:
```
git reset -- fichier1.txt
git checkout -- fichier1.txt
```

Exerice 6:
```
rm fichier1.txt
git add fichier1.txt
git commit -m "Suppresion du fichier1.txt"
git reset HEAD~1
git checkout -- fichier1.txt
```

Exercice 7:
```

```
Exercice 8:
```

```
Exercice 9:
```

```
Exercice 10:
```

```
Exercice 11:
```

```

Exercice 12:
```
git checkout old_branch
git branch -m new_branch
git push origin :old_branch
git push --set-upstream origin new_branch
```

Exercice 13:
```
Ajouter la section ci-desous au fichier ~/.gitconfig
[alias]
        lol = log --graph --decorate --pretty=oneline --abbrev-commit
        lola = log --graph --decorate --pretty=oneline --abbrev-commit --all
```
