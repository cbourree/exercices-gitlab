# Exercices GitLab

Exercice 1 : Cloner un repo
- Cloner le repo dans le repertoire de votre choix en le renommant FormationGit
- Se positioner dans le dossier

Exercice 2 : Changer et créer une de branche
- Aller sur la branche exercices
- Créer une branche à partir de exercice1 en la nommant avec votre trigramme en prefix. Ex: CBO_exercices

Exercice 3 : Modifier un fichier
- Ouvrir le fichier2.txt et ajouter une nouvelle ligne
- Effectuer un diff pour voir les modifications effectuées
- Ajouter les fichiers à la staging zone
- Lister les fichiers qui sont dans le repertoire de travail et la staging area
- Commiter la modification pour pousser les modification dans le repo local
- Pousser la modification sur le repo distant (si c'est mon premier push, suivre les instructions de la console)

Exercice 4 : Ajouter et supprimer un fichier
- Ajouter un fichier4.txt
- Supprimer le fichier1.txt
- Ajotuer ces modifications à la staging area


Exercice 5 : Annuler une supression, alors que je n'ai pas commité
Je viens de me rendre compte que je voulais garder le fichier1.txt.
- Annuler la suppression du fichier1.txt


Exercice 6 : Annuler une supression, alors que j'ai commit mes modifications
- Supprimer le fichier1.txt
- Ajouter les modifications à la staging area
- Commiter les modifications
- Annuler la suppression du fichier1.txt


Exercice 7 : Annuler une supression alors que j'ai poussé mes modifications
2 possibilitées :
- Supprimer le fichier1.txt
- Ajouter les modifications à la staging area
- Commiter les modifications
- Pousser les modifications sur votre branche

1 - C'est passé sur develop ou quelqu'un à tiré mes commits

-> Revert le commit qui pose un problème.

2 - C'est encore sur ma branche, je sais que mes modifications n'impacterons personne

->Modifier l'historique de ma branche et pousser avec --force-with-lease

Exercice 8 : Annuler une modification, alors que j'ai commit mes modifications
- Ajouter une ligne dans le ficher2.txt
- Ajouter les modifications à la staging area
- Commiter les modifications
- Annuller l'ajout de la modification

Exercice 9 : Rennomer et fusioner des commit
- Afficher les logs git sous forme d'arbre avec un peu de couleurs (git log log --graph --decorate --pretty=oneline --abbrev-commit --all)
- Ajouter un fichier5.txt
- Ajouter cette modification à la staging area
- Commiter les modifications en nommant le commit "Nom commit 1"
- Renommer le commit en le nommant "J'ai changé d'avis"
- Ajouter un fichier6.txt
- Ajouter cette modification à la staging area
- Commiter les modifications en nommant le commit "Nom autre commit"
- Fussioner les commits à l'aide d'un rebase interactif sur les deux derniers commits

Exercice 10 : L'interruption
- Modifier le fichier5.txt

J'ai du travail en cours, je suis interrompu par un collègue qui veut que corrige un truc sur sa branche.
Sauvegarder votre travail en cours. 2 possibilités :

- Je suis dans un état instable ne souhaite pas commiter dans l'état actuel :

->Stasher votre travail

- Je suis dans un état stable :

 -> je commit mon travail
- (Il ne manque plus qu'a checkout sur la branche de mon collègue)

Exercice 11 : Merge avec conflits
- Créer une nouvelle branche <trigramme>_exercice11
- Placer vous sur cette nouvelle branche
- Ajouter une ligne au début du fichier3.txt
- Ajouter cette modification à la staging area
- Commiter les modifications
- Pousser les modifications sur cette nouvelle branche
- Déplacer vous sur la premier branche que vous avez créer
- Ajouter une ligne au début du fichier3.txt
- Ajouter cette modification à la staging area
- Commiter les modifications
- Pousser les modifications sur votre branche
- Tirer les modifications de <trigramme>_exercice11 sur votre branche actuelle.
- Résoudre le conflit

Exercice 12 : Renommer votre branche, déjà poussé sur le serveur
- Renommber votre branche

Exercice 13 : Créer un alias
 - Faire en sorte que git lola soit un alias pour git log --graph --decorate --pretty=oneline --abbrev-commit --all

Exercice 15 : Se déplacer dans l'arbre
- Se déplacer au troisième commit le plus ancien et créer une branche à partir de là

Exercice bonus :
- Faire un diff entre deux commits
- Retrouver qui à modifier en la deuxieme ligne du fihcier3.txt
- Supprimer un commit avec le rebase interactif







